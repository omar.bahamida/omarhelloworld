﻿using System;

namespace Omar_HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hallo Welt");
            Console.WriteLine("Salam Alaykom");

            // Ausgabe Benutzer 1
            Console.WriteLine("Hello World von XXX");

            // Ausgabe Benutzer 2
            Console.WriteLine("Nǐ hǎo, shìjiè");

            // Ausgabe Benutzer 3
            Console.WriteLine("Hallo Welt von XXX");

            // Ausgabe Benutzer 4
            Console.WriteLine("marhabaan bialealam");

            //Ausgabe Benutzer 5
            Console.WriteLine("Essalam Alykom");

            //Ausgabe Benutzer 6
            Console.WriteLine("maa ssalama");

            //Ausgabe Benutzer 7
            Console.WriteLine("marhaba bikom");

            // warte bis der User eine Taste drueckt
            Console.ReadKey();

        }
    }
}
